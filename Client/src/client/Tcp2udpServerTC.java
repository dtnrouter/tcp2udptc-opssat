package client;
import java.io.*;
import java.net.*;

import java.nio.ByteBuffer;

import java.util.Arrays;

public class Tcp2udpServerTC {
    private static int inTCPPort=21111;
    private static int outUDPort=4559;
    private static String ipFwdAddress="192.168.2.230";
    private static DatagramSocket udpClientSocket;
    private static DatagramPacket udpSendPacket = null;
    private static ServerSocket tcpServerSocket;
    private static Socket clientSocket=null;
    private static DataInputStream inFromTCPClient =null;
    private static byte[] tcpInBuffer = new byte[64000];
    private static byte[] udpOutBuffer = new byte[64000];
    private static byte[] scratchBuffer = new byte[64000];
    private static ByteBuffer bb;
    private static byte MessageType;
    private static int MessageSize;
    private static int BytesRead;
    private static int tcpInReadBytes;
    private static int udpOutPayloadLength;
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_RESET = "\u001B[0m";
    public Tcp2udpServerTC() {
        super();
    }

    public static void main(String[] args) {
        if (args.length==0||args.length==1||args.length==2){
            System.out.println("need 3 arguments [Input TCP port] [Output UDP port] [ip address] to forward\neg java -jar tcp2udp_tc.jar 21111 4550 192.168.2.230");
            return;
            }
        Tcp2udpServerTC udp2tcpClientTC = new Tcp2udpServerTC();
        inTCPPort = Integer.parseInt(args[0]);
        outUDPort = Integer.parseInt(args[1]);
        ipFwdAddress = args[2];
        bb=ByteBuffer.wrap(scratchBuffer);
        try {
            udpClientSocket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("Failed to initialize udp socket ... \nEnding program on" +
                " error :" + e.toString());
            return;
        }
        while (true){
            try {
                tcpServerSocket = new ServerSocket(inTCPPort);
                System.out.println(ANSI_RED+"waiting for connection"+ANSI_RESET);
                clientSocket = tcpServerSocket.accept();
                System.out.println(ANSI_GREEN+"connected client from "+clientSocket.getInetAddress().toString()+ANSI_RESET);
                inFromTCPClient = new DataInputStream(clientSocket.getInputStream());
            } catch (SocketException e) {
                System.out.println("Failed to initialize tcp socket ... \nEnding program on" +
                    " error :" + e.toString());
                return;
            } catch (UnknownHostException e) {
                System.out.println("Failed to initialize tcp socket ... \nEnding program on" +
                    " error :" + e.toString());
                return;
            } catch (IOException e) {
                System.out.println("Failed to initialize tcp socket ... \nEnding program on" +
                    " error :" + e.toString());
                return;
            }
            try {
                
                while (true){
                    MessageSize=inFromTCPClient.readInt();
                    System.out.println("Message Size: "+MessageSize);
                    BytesRead=0;
                    bb.rewind();
                    Boolean Finished=false;
                    while(!Finished){
                        tcpInReadBytes  = inFromTCPClient.read(tcpInBuffer);
                        System.out.println("read "+tcpInReadBytes+" bytes");
                        bb.put(tcpInBuffer, 0, tcpInReadBytes);
                        BytesRead+=tcpInReadBytes;             
                        System.out.println(" Bytes Read "+tcpInReadBytes);
                        System.out.println(" Bytes until now "+tcpInReadBytes);
                        if (BytesRead==MessageSize){
                            System.out.println("got into if");
                            Finished=true;
                            }
                        }
                    
                    System.out.println(ANSI_GREEN+"Recieved Bytes TCP: "+BytesRead+ANSI_RESET);
                    udpOutPayloadLength = BytesRead;
                    udpOutBuffer = Arrays.copyOf(bb.array(),udpOutPayloadLength );
                    udpSendPacket = new DatagramPacket(udpOutBuffer, 0, udpOutPayloadLength,InetAddress.getByName(ipFwdAddress),outUDPort);
                    udpClientSocket.send(udpSendPacket);
                    System.out.println(ANSI_RED+"Sent     Bytes UDP: "+udpOutPayloadLength+ANSI_RESET);
                    }
            
            }catch (Exception e){
                System.out.println("Failed to read from input socket loop  ... \nEnding program on" +
                                            " error :" + e.toString());
                try {
                    clientSocket.close();
                    return;
                } catch (IOException f) {
                    System.out.println("Failed to close connected client socket ... \nEnding program on" +
                        " error :" + e.toString());
                    return;
                }
            }
        
        }
    }
}



